import os
import shutil
import tempfile
import xml.dom.minidom as minidom
import zipfile
import openai
import time



def is_author_chatGPT(reference):
    promt =f"does the following text include name of author/authors and the year of publication? \n {reference} \n\n text may contain some additional information , please only provide the answer as YES or NO. dont add any extra text to the answer. example : if the text is  'Engineering, Construction and Architectural Management, ' response should be 'NO' because it doesnot contain any person names .\n\n"
    completion1 = openai.ChatCompletion.create(
    model="gpt-3.5-turbo",
    messages=[
        {"role": "user", "content": promt}
    ],
    n=1,
    temperature=1
    )
    result = completion1.choices[0].message.content
    print(reference)
    print("chatResponse :",result.upper())
    sleep_time = 4
    time.sleep(sleep_time) #to avoid rate limit
    if result.upper() == "YES" or result.upper() == "YES.":
        return True
    else:
        return False


def get_word_xml(docx_filename):
    with open(docx_filename,'rb') as f:
        zip = zipfile.ZipFile(f)
        xml_content = zip.read('word/document.xml')
    return xml_content


def write_and_close_docx (xml_content, output_filename):
        """ Create a temp directory, expand the original docx zip.
            Write the modified xml to word/document.xml
            Zip it up as the new docx
        """

        tmp_dir = tempfile.mkdtemp()
        with open(output_filename,'rb') as f:
            zip = zipfile.ZipFile(f)
            filenames = zip.namelist()
            zip.extractall(tmp_dir)

        with open(os.path.join(tmp_dir,'word/document.xml'), 'w',encoding='utf-8') as f:
            f.write(xml_content)

        # Get a list of all the files in the original docx zipfile
        # Now, create the new zip file and add all the filex into the archive
        zip_copy_filename = "mydocx.docx"
        with zipfile.ZipFile(zip_copy_filename, "w") as docx:
            for filename in filenames:
                docx.write(os.path.join(tmp_dir,filename), filename)

        # Clean up the temp dir
        shutil.rmtree(tmp_dir)

def add_newNode(parent_node, line, my_text,color):
    new_line = line.cloneNode(True)
    tag = new_line.getElementsByTagName("w:t")
    tag[0].childNodes[0].nodeValue = my_text
    props_new = new_line.getElementsByTagName("w:rPr")
    col_prop_new = domtree.createElement("w:color")
    col_prop_new.setAttribute("w:val", color)
    props_new[0].appendChild(col_prop_new)
    parent_node.insertBefore(new_line, line)
    return parent_node


if __name__ == "__main__":
    OPENAI_API_KEY = "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"
    openai.api_key = OPENAI_API_KEY
    xml_content = get_word_xml('Research.docx')
    with open('document_my.xml','wb') as f:
        f.write(xml_content)
    domtree = minidom.parse("document_my.xml")
    group = domtree.documentElement
    lines = group.getElementsByTagName("w:r")

    my_list = []

    for line in lines:
        try:
            if line.getAttribute("w:rsidRPr") == "00AB1B8D":
                my_list.append(line)
        except IndexError as e:
            print("IndexError: " + str(e))

    for line in my_list:
        parent_node = line.parentNode
        try:
            tag = line.getElementsByTagName("w:t")
            if tag[0].getAttribute("xml:space") == "preserve":
                reference = tag[0].childNodes[0].nodeValue

                #getting DATA from ChatGPT whether the reference is author or not
                is_author = is_author_chatGPT(reference)

                props = line.getElementsByTagName("w:rPr")

                if is_author:
                    color1 = "00AA00"
                    color2 = "FF0000"
                    try:
                        reference = str(reference.strip())
                        if reference[-1] == '.':
                            reference = reference[:-1]
                        if "." in reference:

                            reference = reference.rsplit(". ", 1)
                            reference[0] = reference[0] + ". "
                            reference[1] = reference[1] + ". "
                            parent_node = add_newNode(parent_node, line, reference[0],color1)
                            parent_node = add_newNode(parent_node, line, reference[1],color2)
                            parent_node.removeChild(line) 
                        else:
                            col_prop = domtree.createElement("w:color")
                            col_prop.setAttribute("w:val", color1)
                            props[0].appendChild(col_prop)
                    except IndexError as e:
                        col_prop = domtree.createElement("w:color")
                        col_prop.setAttribute("w:val", color1)
                        props[0].appendChild(col_prop)

                else:
                    col_prop = domtree.createElement("w:color")
                    col_prop.setAttribute("w:val", "0000FF")
                    props[0].appendChild(col_prop)    

                #adding new text if required
                # my_text = "my_text"
                # color = "FF0000"
                # add_newNode(parent_node, line, my_text,color)
        except IndexError as e:
            print("IndexError: " + str(e))

    write_and_close_docx(domtree.toprettyxml(),"Research.docx")

